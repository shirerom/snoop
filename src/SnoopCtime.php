<?php

class SnoopCtimeExc extends Exception {}
class SnoopCtimeCompareExc extends SnoopCtimeExc {}
class SnoopCtimeOperatorExc extends SnoopCtimeExc {}



/**
 * SnoopCtime funktioniert wie Snoop mit dem Zusatz, dass man Dateien anhand
 * der letzten Modifikation aus- bzw. einschließen kann
 *
 * Anwendungsbeispiel:
 *
 *	$Snoop = new SnoopCtime("/home/codepfuscher");
 *	$Snoop->addExtension("js");
 *	$Snoop->addExtension("php");
 *	$Snoop->scanRecursively();
 *	$Snoop->setCtimeOperator("<=");  // SnoopCtime <= FileCtime
 *	$Snoop->scan();
 *
 *	foreach($Snoop as $file) {
 *		var_dump($file);
 *	}
 */
class SnoopCtime extends Snoop {

	/**
	 * Wenn true, dann wird die Verzeichnisstruktur ab $basePath rekursiv
	 * durchsucht
	 *
	 * Default: false
	 *
	 * @var boolean
	 */
	protected $ctime;

	/**
	 * enthält die akzeptierten Rückgabewerte von compareCtime(). Der Standardwert
	 * bezeichnet "kleiner-gleich"
	 * 
	 * Default: array(-1, 0);
	 *
	 * @var string
	 */
	protected $ctimeOperator;

	/**
	 * @param string $basePath Verzeichnis von dem die Suche ausgeht
	 */
	public function  __construct($basePath) {

		parent::__construct($basePath);
		
		$this->scanRecursively = true;
		$this->cutBasePath = true;
		$this->ctime = time();
		$this->ctimeOperator = array(-1, 0);
	}

	/**
	 * verarbeitet gefundene Dateien
	 *
	 * @param string $file
	 */
	protected function handleFile(&$file) {

		// TS der letzten Änderung vergleichen
		$ctimeComparison = $this->compareCtime($this->ctime, filectime($file));

		if(!in_array($ctimeComparison, $this->ctimeOperator)) {
			return;
		}

		parent::handleFile($file);
	}

	/**
	 * setzt $ctime
	 * 
	 * @param int $timestamp 
	 */
	public function setCtime($timestamp) {
		$this->ctime = (int)$timestamp;
	}


	/**
	 * setzt den Operator für den Vergleich der ctime
	 * 
	 * @param string $operator erlaubte Werte: <, >, <=, >=
	 *
	 * @throws SnoopCtimeOperatorExc wenn das erste Zeichen in Operator nicht "<" oder ">" ist
	 */
	public function setCtimeOperator($operator) {

		// zurücksetzen
		$this->ctimeOperator = array();

		// größer oder kleiner
		if(strcmp($operator, "<") === 0) {
			$this->ctimeOperator[] = -1;
		}

		else if(strcmp($operator, ">") === 0 ) {
			$this->ctimeOperator[] = -1;
		}

		else {
			throw new SnoopCtimeOperatorExc($operator);
		}

		// ist gleich
		if(strlen($operator) == 2 and strcmp($operator, "=") == 1) {
			$this->ctimeOperator[] = 0;
		}
	}

	/**
	 * vergleicht 2 Timestamps
	 *
	 * @param int $timestamp1
	 * @param int $timestamp2
	 *
	 * @return int <br>
	 *	-1 wenn T1 < T2,
	 *	0 wenn T1 == T2,
	 *	1 wenn T1 > T2
	 *
	 * @throws SnoopCtimeCompareExc, wenn keiner der Vergleiche zutrifft
	 */
	protected function compareCtime($timestamp1, $timestamp2) {

		$timestamp1 = (int)$timestamp1;
		$timestamp2 = (int)$timestamp2;

		if($timestamp1 == $timestamp2) {
			return 0;
		}

		elseif($timestamp1 < $timestamp2) {
			return -1;
		}

		elseif($timestamp1 > $timestamp2) {
			return 1;
		}

		else {
		 throw new SnoopCtimeCompareExc();
		}
	}

}
?>
